import React from 'react';
import PropTypes from 'prop-types';

//import Text from 'text';
//import Img from 'img';
//import RichText from 'richtext';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './Figure.scss';

/**
 * Figure
 * @description [description]
 * @example
  <div id="Figure"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Figure, {
	}), document.getElementById("Figure"));
  </script>
 */
class Figure extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'figure';
	}

	render() {
		const {image, caption, children} = this.props;

		const content = typeof children === 'string' ? <RichText content={children} /> : children;

		if ((!image || (image && !image.src)) && !content) {
			return null;
		}

		return (
			<figure className={this.baseClass}>
				{content}
				<Img {...image} />
				<Text tag="figcaption" content={caption} className={`${this.baseClass}__caption`} />
			</figure>
		);
	}
}

Figure.defaultProps = {
	image: null,
	caption: '',
	children: ''
};

Figure.propTypes = {
	caption: PropTypes.string,
	children: PropTypes.node,
	image: PropTypes.object
};

export default Figure;
